import tensorflow as tf



mnist = tf.keras.datasets.mnist #On DL le dataset MNIST

(x_train, y_train),(x_test, y_test) = mnist.load_data() #On charge les données d'entrainements
x_train, x_test = x_train / 255.0, x_test / 255.0 

model = tf.keras.models.Sequential([ #Génération des couches du neural network
  tf.keras.layers.Flatten(input_shape=(28, 28)),
  tf.keras.layers.Dense(512, activation=tf.nn.relu),
  tf.keras.layers.Dropout(0.2),
  tf.keras.layers.Dense(10, activation=tf.nn.softmax)
])
model.compile(optimizer='adam', #Configure le model pour l'entrainement 
              loss='sparse_categorical_crossentropy',
              metrics=['accuracy'])


print(mnist.load_data())


model.fit(x_train, y_train, epochs=3) #Nombre d'itérations : 3
model.evaluate(x_test, y_test) #Retourn la "perte" (loss) du model 

model.save('test_MNIST') # On enregistre le model 


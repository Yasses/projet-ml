import tensorflow as tf
import matplotlib.pyplot as plt
import numpy as np


mnist = tf.keras.datasets.mnist

(x_train, y_train),(x_test, y_test) = mnist.load_data()
x_train, x_test = x_train / 255.0, x_test / 255.0


new_model = tf.keras.models.load_model('test_MNIST')
predictions = new_model.predict([x_test])

v = 40


print("La prédiction est : ", np.argmax(predictions[v]))

plt.imshow(x_test[v])
plt.show()


import sys
import os
import tensorflow as tf
import wave
import cv2
import pylab
import numpy as np



def graph_spectrogram(wav_file): # Fonction qui créer un spectrogram à partir d'un fichier .wav (audio)
	sound_info, frame_rate = get_wav_info(wav_file)
	pylab.figure(num=None, figsize=(19, 12))
	pylab.subplot(111)
	pylab.title('spectrogram of %r' % wav_file)
	pylab.specgram(sound_info, Fs=frame_rate)
	pylab.savefig('spectrogram_test.png')
	pylab.close('all')


def get_wav_info(wav_file): # Fonction qui renvoie les informations d'un audio 
	wav = wave.open(wav_file, 'r')
	frames = wav.readframes(-1)
	sound_info = pylab.fromstring(frames, 'Int16')
	frame_rate = wav.getframerate()
	wav.close()
	return sound_info, frame_rate



son_dir = sys.argv[1] # Je récupere le 1er arguments passé en ligne de commande au lancement du programme, qui est le chemin vers le son 

os.system('ffmpeg -y -i '+son_dir+'.wav -ar 44100 resample_test.wav') # J'utilise la commande windows ffmpeg pour rééchantilloné le son car certain son sont au dessus de 48 000 Hz, qui n'est pas supporté par python 

graph_spectrogram('resample_test.wav') # Je créer le spectrogram du son rééchantilloné


img_array = cv2.imread('spectrogram_test.png' ,cv2.IMREAD_GRAYSCALE)  # On fait de l'image une matrice
new_array = img_array[180:1050, 250:1700] # On crop l'image
X = cv2.resize(new_array,(87,145)) # on redimmensionne l'image
X = np.array(X).reshape(-1, 87, 145, 1)

X = X/255. # On passe les 256 nuances de gris entre 0 et 1
new_model = tf.keras.models.load_model('processed data+trained_model\\Test_UrbanSounds_51%_Acc') # On charge le modele pré entrainé 
predictions = new_model.predict(X) # Il y a maintenant une liste des prédictions du model 
Z = np.argmax(predictions) # Z est maintenant la valeur numérique avec la plus haute prédiction


# Avec le code du dessous, on ne fait que de l'affichage pour la mieux comprendre ce qui se passe à l'ecran au lancement 

print(" ")
print("-------------------------------------------------------------------")
print('Climatisation : ', predictions[0][0]*100, '%')
print('Klaxon : ', predictions[0][1]*100, '%')
print('Enfants : ', predictions[0][2]*100, '%')
print('Chien : ', predictions[0][3]*100, '%')
print('Perceuse : ', predictions[0][4]*100, '%')
print('Moteur : ', predictions[0][5]*100, '%')
print('Coup de feu : ', predictions[0][6]*100, '%')
print('Marteau piqueur : ', predictions[0][7]*100, '%')
print('Sirène : ', predictions[0][8]*100, '%')
print('Musique de rue : ', predictions[0][9]*100, '%')
print("-------------------------------------------------------------------")
print(" ")

if Z == 0:
	print("C'est une Climatisation")
elif Z == 1:
	print("C'est un Klaxon")
elif Z == 2:
	print("C'est un Enfant")
elif Z == 3:
	print("C'est un Chien")
elif Z == 4:
	print("C'est une Perceuse")
elif Z == 5:
	print("C'est un Moteur")
elif Z == 6:
	print("C'est un Coup de feu")
elif Z == 7:
	print("C'est un Marteau piqueur")
elif Z == 8:
	print("C'est une Sirène")
else:
	print("C'est de la Musique de rue")
import os
import wave
import pylab
import ffmpy
import csv

def graph_spectrogram(wav_file,i,classe): # Cette classe prend un fichier audio et le transforme en spectrograme tout en l'enregistrant dans un dossier selon sa classe
	sound_info, frame_rate = get_wav_info(wav_file)
	pylab.figure(num=None, figsize=(19, 12))
	pylab.subplot(111)
	pylab.title('spectrogram of %r' % wav_file)
	pylab.specgram(sound_info, Fs=frame_rate)
	pylab.savefig('Resample\\Spectrogram\\Classes\\'+classe+'\\spectrogram_'+str(i)+'_.png')
	pylab.close('all')

def get_wav_info(wav_file): # Cette fonction retourne les information d'un fichier audio (.wav)
	wav = wave.open(wav_file, 'r')
	frames = wav.readframes(-1)
	sound_info = pylab.fromstring(frames, 'Int16')
	frame_rate = wav.getframerate()
	wav.close()
	return sound_info, frame_rate


if __name__ == '__main__':
	#wav_file = 'sample.wav'
	#graph_spectrogram(wav_file)
	with open('train.csv') as csvfile: # On ouvre le fichier excel qui a chaque ligne a l'index d'un son avec sa classe
		readCSV = csv.reader(csvfile, delimiter=',')
		for row in readCSV:
				i = row[0]
				os.system('ffmpeg -i '+str(i)+'.wav -ar 44100 Resample\\'+str(i)+'.wav') # On rééchatillonne tout les sons avec la commande windows(cmd) ffmpeg car certains sons sont à plus de 48000Hz qui est non supporté par python
				wav_file = 'Resample\\'+str(i)+'.wav'
				graph_spectrogram(wav_file,i, row[1])




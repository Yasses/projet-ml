import numpy as np
import matplotlib.pyplot as plt
import os
import cv2
import random
import pickle

DATADIR = "C:\\Users\\Yassine\\Documents\\ML\\Datasets\\Spectro_Urban_Sounds_tri"

CATEGORIES = ["air_conditioner", "car_horn","children_playing","dog_bark","drilling","engine_idling","gun_shot","jackhammer","siren","street_music"] # On configure une liste avec le nom de toute les classes

training_data = []


def create_training_data():
	for category in CATEGORIES: 
		path = os.path.join(DATADIR,category)  # créer le chemin vers nos classes
		class_num = CATEGORIES.index(category) # On donne à chaque catégorie une valeur numérique (son index) car le cerveau neuronal ne prend que des valeur numérique 

		for img in os.listdir(path):  # itération sur toute les images
			try :
				img_array = cv2.imread(os.path.join(path,img) ,cv2.IMREAD_GRAYSCALE)  # On fait de l'image une matrice en nuance de gris 
				new_array = img_array[180:1050, 250:1700] # On crop l'image pour ne prendre que le spectrogramme sans l'échelle et le nom du fichier 
				new_array = cv2.resize(new_array,(87,145)) # on redimmensionne l'image pour que mon PC puissent supporter 
				training_data.append([new_array, class_num]) # on créer la liste des données labelisé 

			except Exception as e:
				pass



create_training_data()
print(len(training_data))
random.shuffle(training_data)

X = []
y = []

for data, label in training_data:
	X.append(data)
	y.append(label)

X = np.array(X).reshape(-1, 87, 145, 1)

pickle_out = open("X.pickle","wb") # Pickle nous servira à enregistrer les listes de matrices et de labels pour ne pas les recalculer à chaque fois 
pickle.dump(X, pickle_out)
pickle_out.close

pickle_out = open("y.pickle","wb")
pickle.dump(y, pickle_out)
pickle_out.close
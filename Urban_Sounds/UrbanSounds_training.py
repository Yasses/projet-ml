import numpy as np
import matplotlib.pyplot as plt
import pickle
import tensorflow as tf
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Dropout, Activation, Flatten, Conv2D, MaxPooling2D


pickle_in = open("X.pickle","rb") # On recharge les listes précedemment enregistré avec Pickle
X = pickle.load(pickle_in)

pickle_in = open("y.pickle","rb")
y = pickle.load(pickle_in)

X = X/255.0 # On passe les 256 nuances de gris entre 0 et 1



model = Sequential([ #Génération des couches du neural network

	Conv2D(256, (3, 3), input_shape=X.shape[1:]), # Premiere couche convolutionnel
	Activation('relu'),
	MaxPooling2D(pool_size=(2, 2)),

	Conv2D(256, (3, 3)), # Deuxieme couche convolutionnel
	Activation('relu'),
	MaxPooling2D(pool_size=(2, 2)),

	Flatten(), # On "applati les valeurs", on réduit la dimension
	Dropout(0.1), # Cette couche rajoute de l'aléatoire, car j'avais de l'overfitting
	Dense(64),
	Dense(10), # Couche de sortie avec 10 neurones pour les 10 classes
	Activation('softmax')
])



model.compile(loss='sparse_categorical_crossentropy',
              optimizer='adam',
              metrics=['accuracy'])

model.fit(X, y, batch_size=32, epochs=8)

model.save("Test_UrbanSounds") # On enregistre le modele entrainé